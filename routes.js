const reportOPT = {
  schema: {
    response: {
      200: {
        type: 'array',
        items: {
          type: 'object',
          properties: {
            name: { type: 'string' },
            _id: { type: 'string' },
            sub1: { type: 'string' },
            sub2: { type: 'string' },
            sub3: { type: 'string' },
            sub4: { type: 'string' },
            sub5: { type: 'string' }

          }
        }
      }
    }
  }
}
const addOPT = {
  schema: {
    body: {
      type: 'array',
      items: {
        type: 'object',
        properties: {
          name: { type: 'string' },
          _id: { type: 'string' },
          sub1: { type: 'string' },
          sub2: { type: 'string' },
          sub3: { type: 'string' },
          sub4: { type: 'string' },
          sub5: { type: 'string' }

        }
      }
    }

  }
}
const updateOPT = {
  schema: {

    body: {
      type: 'array',
      items: {
        type: 'object',
        properties: {
          name: { type: 'string' },
          _id: { type: 'string' },
          sub1: { type: 'string' },
          sub2: { type: 'string' },
          sub3: { type: 'string' },
          sub4: { type: 'string' },
          sub5: { type: 'string' }

        }
      }
    }

  },
  response: {
    200: {
      type: 'object',
      properties: {
        name: { type: 'string' },
        _id: { type: 'string' },
        sub1: { type: 'string' },
        sub2: { type: 'string' },
        sub3: { type: 'string' },
        sub4: { type: 'string' },
        sub5: { type: 'string' }

      }

    }
  }
}

const deleteOPT = {
  schema: {
    querystring: {
      id: { type: 'string' }
    },
    response: {
      200: {
        type: 'object',
        properties: {
          status: { type: 'boolean' }
        }
      }
    }
  }
}
function routes (fastify, options) {
  const studentdb = fastify.mongo.db.collection('todo')

  fastify.add('/:id', addOPT, async (request, reply) => {
    const todo = request.body
    await studentdb.insert(todo)
  })
  fastify.update('/', updateOPT, async (request, reply) => {
    const todo = request.body
    const result = await studentdb.insert(todo)
    const insertedTodo = result.ops[0]
    return insertedTodo
  })
  fastify.report('/', reportOPT, async (request, reply) => {
    const sub = await studentdb.find().toArray()
    return sub
  })
  fastify.delete('/:id', deleteOPT, async (request, reply) => {
    const { id } = request.params
    const _id = fastify.mongo.ObjectId(id)
    await studentdb.deleteOne({ _id })
    return { status: true }
  })
}

module.exports = routes
