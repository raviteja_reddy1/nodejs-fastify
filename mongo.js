const fastifyPlugin = require('fastify-plugin')

function dbConnector (fastify, options, done) {
  fastify.register(require('fastify-mongodb'), {
    url: 'mongodb://localhost:27017/test_database'
  })
  done()
}

module.exports = fastifyPlugin(dbConnector)
