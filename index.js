// Require the framework and instantiate it
const fastify = require('fastify')({ logger: true })

// Declare a route
fastify.register(require('./plugin/mongo'))
fastify.register(require('./plugin/routes'), { prefix: '/post' })

// Run the server!
const start = async () => {
  try {
    await fastify.listen(8080)
  } catch (err) {
    console.log(err)
    fastify.log.error(err)
  }
}
start()
